declare module '*.css' {
  interface Style {
    [key: string]: string;
  }

  const style: Style;

  export default style;
}

declare module '*.png' {
  const value: string;

  export default value;
}

declare module '*.svg' {
  const value: string;
  const ReactComponent: any;

  export { ReactComponent };
  export default value;
}

declare module '*.json' {
  const value: any;

  export default value;
}
