import React from 'react';

import cn from 'classnames';

import { spiner } from '../../assets';

import { ButtonColorSheme } from './model';
import style from './style.css';

interface ButttonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  colorScheme?: ButtonColorSheme;
  onClick?: () => void;
  className?: string;
  loading?: boolean;
}

const Button: React.FC<ButttonProps> = ({
  colorScheme,
  children,
  className,
  loading,
  ...rest
}) => (
  <button {...rest} className={cn(style.main, style[colorScheme], className)}>
    {loading && (
      <span className={style.spinner}>
        <img src={spiner} />
      </span>
    )}
    {children}
  </button>
);

Button.defaultProps = {
  colorScheme: ButtonColorSheme.green,
};

export default Button;
