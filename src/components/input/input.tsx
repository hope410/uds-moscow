import React from 'react';
import { Link as ConnectedLink } from 'react-router-dom';

import cls from 'classnames';

import { Link } from '../../__data__/model';

import style from './style.css';

interface Props {
  id: string | number;
  name: string;
  label?: string;
  type?: string;
  link?: Link;
  inputRef?: React.RefObject<HTMLInputElement>;
  error?: string | boolean;
}

type InputProps = Props &
  Omit<
    React.DetailedHTMLProps<
      React.InputHTMLAttributes<HTMLInputElement>,
      HTMLInputElement
    >,
    'id'
  >;

export const Input: React.FC<InputProps> = ({
  inputRef,
  className,
  label,
  id,
  link,
  name,
  type,
  error,
  ...rest
}) => (
  <div className={cls(style.wrapper, className)}>
    <div className={style.infoBlock}>
      {label && (
        <label className={style.label} htmlFor={String(id)}>
          {label}
        </label>
      )}
      {link && (
        <ConnectedLink className={style.link} to={link.href}>
          {link.label}
        </ConnectedLink>
      )}
      {error && typeof error !== 'boolean' && (
        <div className={style.error}>
          <span>{error}</span>
        </div>
      )}
    </div>

    <input
      className={cls(style.field, error && style.alert)}
      id={String(id)}
      name={name}
      ref={inputRef}
      type={type}
      {...rest}
    />
  </div>
);

Input.defaultProps = {
  type: 'text',
  error: void 0,
};
