import React from 'react';

import style from './style.css';

interface LayoutProps {
  children: any;
}

export default function Layout(props: LayoutProps) {
  return <div className={style.layout}>{props.children}</div>;
}
