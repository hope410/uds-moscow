import React, { Suspense } from 'react';

import ErrorBoundary from '../error-boundary';

export const LazyComponent = ({ children, fallback }) => (
  <ErrorBoundary>
    <Suspense fallback={fallback}>{children}</Suspense>
  </ErrorBoundary>
);

LazyComponent.defaultProps = {
  fallback: 'Loading...',
};
