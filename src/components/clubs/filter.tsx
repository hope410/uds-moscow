import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import fetchRegions from '../../__data__/actions/fetch-regions';
import { RootState } from '../../__data__/store/reducers';
import { SelectInput, SelectOptionsGroup, SelectOption } from '../select-input';

import style from './style.css';

const Filter = () => {
  const dispatch = useDispatch();
  const groups = useSelector<RootState, SelectOptionsGroup[]>(
    (state) =>
      state.regions.data?.map((item) => ({
        title: item.title,
        options:
          item.districts?.map((dist) => ({
            value: dist.id,
            label: dist.title,
          })) || [],
      })) || []
  );
  const [region, setRegion] = useState<SelectOption | null>(null);

  useEffect(() => {
    fetchRegions(dispatch)();
  }, []);

  return (
    <div className={style.filter}>
      <div className="label">Округ или Район</div>
      <SelectInput
        groups={groups}
        value={region?.value}
        label={region?.label}
        onSelect={(item) => setRegion(item)}
      />
    </div>
  );
};

export { Filter };
