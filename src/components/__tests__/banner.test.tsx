import React from 'react';
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals';

import { Banner } from '../banner';
import { ReactComponent as img } from '../../assets/images/logo.svg';

describe('<Banner />', () => {
  it('Отрисовывается без ошибок', () => {
    const wrapper = mount(<Banner src={img} />);

    expect(wrapper.find('div')).toMatchSnapshot();
    expect(wrapper.find('img')).toMatchSnapshot();
  });

  it('Пробрасывает цвет фона', () => {
    const wrapper = mount(<Banner src={img} bgColor="А вот и цвет фона" />);

    expect(wrapper).toMatchSnapshot();
  });
});
