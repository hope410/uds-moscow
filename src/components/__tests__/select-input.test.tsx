import React from 'react';
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals';

import { SelectInput, SelectOptionsGroup } from '../select-input/select-input';

describe('<SelectInput />', () => {
  it('Функционирует без ошибок', () => {
    const onSelect = jest.fn();

    const groups: SelectOptionsGroup[] = [
      {
        title: 'Test',
        options: [
          {
            label: 'test1',
            value: 'test1',
          },
          {
            label: 'test2',
            value: 'test2',
          },
          {
            label: 'test3',
            value: 'test3',
          },
        ],
      },
    ];

    const wrapper = mount(
      <SelectInput
        value={groups[0].options[0].value}
        label={groups[0].options[0].label}
        groups={groups}
        onSelect={onSelect}
      />
    );

    expect(wrapper).toMatchSnapshot();

    expect(
      wrapper.find('[data-cy="select-input-container"]').exists()
    ).toBeTruthy();
    wrapper.find('[data-cy="select-input-container"]').simulate('click');
    wrapper.update();

    expect(wrapper).toMatchSnapshot();

    expect(
      wrapper.find('[data-cy="select-input-dropdown"]').exists()
    ).toBeTruthy();
    expect(wrapper.find('div#select-input-group__0').exists()).toBeTruthy();

    wrapper
      .find('div#select-input-group__0 > [data-cy="select-input-group-header"]')
      .simulate('click');
    wrapper.update();

    expect(wrapper).toMatchSnapshot();

    expect(
      wrapper
        .find(
          'div#select-input-group__0 > [data-cy="select-input-group-items"]'
        )
        .exists()
    ).toBeTruthy();

    expect(
      wrapper
        .find(
          'div#select-input-group__0 > [data-cy="select-input-group-items"] > [data-cy="select-input-item"]#select-input-item__0'
        )
        .exists()
    ).toBeTruthy();

    wrapper.find('div#select-input-item__1').simulate('click');
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
    expect(onSelect).toHaveBeenCalledWith(groups[0].options[1]);
  });
});
