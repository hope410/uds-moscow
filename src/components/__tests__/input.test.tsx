import React from 'react';
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals';

import { Input } from '../input/input';

describe('<Input />', () => {
  it('Функционирует без ошибок', () => {
    const onChange = jest.fn();

    const wrapper = mount(
      <Input
        id="test-input"
        name="test-input"
        value=""
        onChange={(evt) => onChange(evt.target.value)}
      />
    );

    expect(wrapper).toMatchSnapshot();

    const event = {
      target: {
        value: 'test',
      },
    };

    wrapper.find('input#test-input').simulate('change', event);

    expect(onChange).toHaveBeenCalledWith(event.target.value);

    expect(wrapper).toMatchSnapshot();
  });
});
