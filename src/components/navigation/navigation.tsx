import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import cn from 'classnames';

import { URLs } from '../../__data__/urls';
import PhoneIcon from '../../assets/icons/phone.svg';
import Logo from '../../assets/images/logo.svg';

import style from './style.css';

const Navigation = ({ history, location }: RouteComponentProps<any>) => {
  const tabs = [
    {
      name: 'Главная',
      path: URLs.root.url,
    },
    {
      name: 'Учреждения',
      path: URLs.institutions.url,
    },
    {
      name: 'Секции',
      path: URLs.sections.url,
    },
  ];

  const onTabClick = (path: string) => {
    history.push(path);
  };

  return (
    <div className={style.navigation}>
      <div className={style.logo}>
        <img src={Logo} />
      </div>
      <div className={style.right}>
        <div className={style.tabs}>
          {tabs.map((tab, idx) => (
            <div
              className={cn(
                style.tab,
                location.pathname === tab.path && style.tabActive
              )}
              key={`tab_${idx}`}
              onClick={() => onTabClick(tab.path)}
            >
              {tab.name}
            </div>
          ))}
        </div>
        <div className={style.phone}>
          <div className={style.phoneIcon}>
            <img src={PhoneIcon} />
          </div>
          <div className={style.phoneNumber}>8 (495) 640-91-57</div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Navigation);
