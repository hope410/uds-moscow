import React, { useState } from 'react';
import cn from 'classnames';

import { SelectOption, SelectOptionValue } from './select-input';

import style from './style.css';

interface SelectInputGroupProps {
  id: string;
  value: SelectOptionValue;
  title: string;
  options: SelectOption[];
  onSelect: (item: SelectOption) => void;
}

const SelectInputGroup = (props: SelectInputGroupProps) => {
  const [isOpened, setIsOpened] = useState(false);
  return (
    <div
      id={props.id}
      data-cy="select-input-group"
      className={style.selectInputGroup}
    >
      <div
        className={style.selectInputGroupHeader}
        data-cy="select-input-group-header"
        onClick={() => setIsOpened(!isOpened)}
      >
        <div
          data-cy="select-input-group-control"
          className={style.selectInputGroupControl}
        >
          {isOpened ? '-' : '+'}
        </div>
        <div
          data-cy="select-input-group-title"
          className={style.selectInputGroupTitle}
        >
          {props.title}
        </div>
      </div>
      {isOpened && (
        <div
          data-cy="select-input-group-items"
          className={style.selectInputGroupItems}
        >
          {props.options.map((item, idx) => (
            <div
              id={`select-input-item__${idx}`}
              data-cy="select-input-item"
              className={cn(
                style.selectInputGroupOption,
                props.value === item.value && style.selectInputGroupOptionActive
              )}
              key={item.value}
              onClick={() => props.onSelect(item)}
            >
              {item.label}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export { SelectInputGroup };
