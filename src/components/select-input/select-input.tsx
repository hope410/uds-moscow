import React, { useState } from 'react';

import { ReactComponent as ArrowDownIcon } from '../../assets/icons/arrow-down.svg';
import { ReactComponent as ArrowUpIcon } from '../../assets/icons/arrow-up.svg';
import { SelectInputGroup } from './select-input-group';

import style from './style.css';

export type SelectOptionValue = string | number;

interface SelectOption {
  value: SelectOptionValue;
  label: string;
}

interface SelectOptionsGroup {
  title: string;
  options: SelectOption[];
}

interface SelectInputProps {
  value: SelectOptionValue;
  label: string;
  groups: SelectOptionsGroup[];
  onSelect: (item: SelectOption) => void;
}

const SelectInput = (props: SelectInputProps) => {
  const [isOpened, setIsOpened] = useState(false);
  return (
    <div data-cy="select-input" className={style.selectInput}>
      <div
        data-cy="select-input-container"
        className={style.inputContainer}
        onClick={() => setIsOpened(!isOpened)}
      >
        <input
          data-cy="select-input-label"
          type="text"
          className={style.input}
          value={props.label}
          onChange={() => null}
        />
        <div className={style.icon}>
          {isOpened ? <ArrowUpIcon /> : <ArrowDownIcon />}
        </div>
      </div>
      <div className={style.dropdownContainer}>
        {isOpened && (
          <div data-cy="select-input-dropdown" className={style.dropdown}>
            {props.groups.map((item, idx) => (
              <SelectInputGroup
                id={`select-input-group__${idx}`}
                value={props.value}
                key={idx}
                options={item.options}
                title={item.title}
                onSelect={(item) => props.onSelect(item)}
              />
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export { SelectInput, SelectOptionsGroup, SelectOption };
