import './app.css';

import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { store } from './__data__/store';
import Layout from './components/layout/layout';
import Navigation from './components/navigation';
import Dashboard from './containers/dashboard';

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Navigation />
      <Layout>
        <Dashboard />
      </Layout>
    </BrowserRouter>
  </Provider>
);

export default App;
