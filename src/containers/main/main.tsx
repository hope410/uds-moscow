import React from 'react';
import ClubsAndStudies from './blocks/clubs-and-studies/clubs-and-studies';
import Social from './blocks/social/social';
import Competitions from './blocks/competitions';
import Information from './blocks/information/information';
import Footer from './blocks/footer';
import Littleones from './blocks/littleones/littleones';
import style from './style.css';

export default function Main() {
  return (
    <div className={style.container}>
      <Littleones />
      <Information />
      <ClubsAndStudies />
      <Social />
      <Competitions />
      <Footer />
    </div>
  );
}
