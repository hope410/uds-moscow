import React from 'react';
// import play_btn from '../../../../assets/images/play_btn.png';
import style from './style.css';

const Information = () => {
  return (
    <div className={style.container}>
      <div className={style.player}></div>
      <div className={style.information}>
        <div className={style.title}>
          Что такое портал учереждений досуга и спорта uds-moscow?
        </div>
        <div className={style.text}>
          Удивляют низкие цены и смущает, что множество спортивных секций и
          творческих кружков бесплатные? Непонятно как такое может быть?
          Никакого обмана! Давайте знакомиться, посмотрите видео и если
          останутся вопросы, позвоните нам, мы готовы принять ваш звоник 24/7 и
          ответить на все вопросы. Мы предложим спортивные, творческие и
          развивающие кружки и секции для детей и взрослых любого возраста.
          Количество мест может быть ограничено.
        </div>
      </div>
    </div>
  );
};

export default Information;
