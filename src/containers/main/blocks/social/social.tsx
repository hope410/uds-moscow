import React from 'react';

import LogoFacebook from '../../../../assets/icons/facebook-colored.svg';
import LogoInstagram from '../../../../assets/icons/instagram-colored.svg';
import LogoOdnoklassniki from '../../../../assets/icons/odnoklassniki-colored.svg';
import LogoTwitter from '../../../../assets/icons/twitter-colored.svg';
import LogoVk from '../../../../assets/icons/vk-colored.svg';

import style from './style.css';

const Social = () => {
  return (
    <div className={style.social}>
      <div className={style.socialTop}>Мы в социальных сетях</div>
      <div className={style.socialBottom}>
        <img className={style.socialLogo} src={LogoFacebook} alt="" />
        <img className={style.socialLogo} src={LogoInstagram} alt="" />
        <img className={style.socialLogo} src={LogoOdnoklassniki} alt="" />
        <img className={style.socialLogo} src={LogoTwitter} alt="" />
        <img className={style.socialLogo} src={LogoVk} alt="" />
      </div>
    </div>
  );
};

export default Social;