import React from 'react';

import LogoFacebook from '../../../../assets/icons/facebook.svg';
import LogoInstagram from '../../../../assets/icons/instagram.svg';
import LogoOdnoklassniki from '../../../../assets/icons/odnoklassniki.svg';
import PhoneIcon from '../../../../assets/icons/phone-icon.svg';
import LogoTwitter from '../../../../assets/icons/twitter.svg';
import LogoVk from '../../../../assets/icons/vk.svg';
import Logogray from '../../../../assets/images/logo-gray.svg';

import style from './style.css';

const Footer = () => {
  return (
    <div className={style.footer}>
      <div className={style.footerGroup}>
        <div className={style.logo}>
          <img alt="logo" src={Logogray} />
        </div>
      </div>
      <div className={style.footerGroup}>
        <div className={style.phone}>
          <div className={style.phoneIcon}>
            <img src={PhoneIcon} />
          </div>
          <div className={style.phoneNumber}>
            8 (495) 640-91-57
          </div>
        </div>
        <div className={style.social}>
          <div className={style.vk}>
            <a href="#"><img alt="logoVk" src={LogoVk} /></a>
          </div>
          <div className={style.instagram}>
            <a href="#"><img alt="logoInstagram" src={LogoInstagram} /></a>
          </div>
          <div className={style.odnoklassniki}>
            <a href="#"><img alt="logoOdnoklassniki" src={LogoOdnoklassniki} /></a>
          </div>
          <div className={style.facebook}>
            <a href="#"><img alt="logoFacebook" src={LogoFacebook} /></a>
          </div>
          <div className={style.twitter}>
            <a href="#"><img alt="logoTwitter" src={LogoTwitter} /></a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
