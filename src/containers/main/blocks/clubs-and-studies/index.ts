import { lazy } from 'react';

export default lazy(() => import('./clubs-and-studies'));
