import React from 'react';
import { Clubs } from '../../../../components/clubs';

import style from './style.css';

const ClubsAndStudies = () => {
  return (
    <div data-cy="clubs-and-studies" className={style.container}>
      <Clubs />
    </div>
  );
};

export default ClubsAndStudies;
