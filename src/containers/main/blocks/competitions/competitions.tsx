import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import fetchCompetitions from '../../../../__data__/actions/fetch-competitions';
import Competition from '../../../../__data__/models/Competition';
import { RootState } from '../../../../__data__/store/reducers';

import style from './style.css';

const Competitions = () => {
  const dispatch = useDispatch();
  const competitions = useSelector<RootState, Competition[]>(
    (state) => state.competitions.data || []
  );

  useEffect(() => {
    fetchCompetitions(dispatch)();
  }, []);

  return (
    <div data-cy="competitions" className={style.container}>
      <div className={style.title}>Соревнования</div>
      <div className={style.competitions}>
        {competitions.map((item, idx) => (
          <div className={style.competitionItem} key={`comp_item_${idx}`}>
            <div className={style.competitionItem__date}>{item.date}</div>
            <div className={style.competitionItem__month}>{item.month}</div>
            <div className={style.competitionItem__point}></div>
            <div className={style.competitionItem__title}>{item.title}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Competitions;
