import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { URLs } from '../__data__/urls';
import LazyComponent from '../components/lazy-component';

import Institutions from './institutions/institutions';
import Join from './join/join';
import Main from './main/main';
import Sections from './sections/sections';

const Dashboard = () => (
  <Switch>
    <Route path={URLs.institutions.url}>
      <LazyComponent>
        <Institutions />
      </LazyComponent>
    </Route>
    <Route path={URLs.sections.url}>
      <LazyComponent>
        <Sections />
      </LazyComponent>
    </Route>
    <Route path={URLs.join.url}>
      <LazyComponent>
        <Join />
      </LazyComponent>
    </Route>
    <Route path={URLs.root.url}>
      <LazyComponent>
        <Main />
      </LazyComponent>
    </Route>

    <Route path="*">
      <h1>Not Found</h1>
    </Route>
  </Switch>
);

export default Dashboard;
