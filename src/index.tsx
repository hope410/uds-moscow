import React from 'react';
import ReactDom from 'react-dom';

import App from './app';

export const mount = (Сomponent) => {
  ReactDom.render(<Сomponent />, document.getElementById('app'));
};

export const unmount = () => {
  ReactDom.unmountComponentAtNode(document.getElementById('app'));
};

const Index = () => <App />;
export default Index;
