import District from '../District';

export default interface Region {
  title: string;
  districts: District[];
}
