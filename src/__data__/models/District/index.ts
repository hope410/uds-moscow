export default interface District {
  id: string;
  title: string;
}
