export default interface Competition {
  title: string;
  date: number;
  month: string;
}
