import { getConfigValue } from '@ijl/cli';
import { Dispatch } from 'react';

import { REGIONS_GET_DATA } from '../action-types';
import Region from '../models/Region';
import { RegionsAction } from '../store/reducers/regions';

const baseApiUrl = getConfigValue('uds-moscow.api');

const setRegionsData = (data: Region[]): RegionsAction => ({
  type: REGIONS_GET_DATA,
  payload: { data },
});

const fetchRegions = (dispatch: Dispatch<RegionsAction>) => async () => {
  try {
    const data: Region[] = await fetch(`${baseApiUrl}/regions`).then((res) =>
      res.json()
    );

    dispatch(setRegionsData(data));
  } catch (e) {
    console.error(e);
  }
};

export default fetchRegions;
