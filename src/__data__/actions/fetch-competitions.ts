import { getConfigValue } from '@ijl/cli';
import { Dispatch } from 'react';

import { COMPETITIONS_GET_DATA } from '../action-types';
import Competition from '../models/Competition';
import { CompetitionsAction } from '../store/reducers/competitions';

const baseApiUrl = getConfigValue('uds-moscow.api');

const setCompetitionsData = (data: Competition[]): CompetitionsAction => ({
  type: COMPETITIONS_GET_DATA,
  payload: { data },
});

const fetchCompetitions = (
  dispatch: Dispatch<CompetitionsAction>
) => async () => {
  try {
    const data: Competition[] = await fetch(
      `${baseApiUrl}/competitions`
    ).then((res) => res.json());

    dispatch(setCompetitionsData(data));
  } catch (e) {
    console.error(e);
  }
};

export default fetchCompetitions;
