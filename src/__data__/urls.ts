import { getNavigations } from '@ijl/cli';

const navigations = getNavigations('uds-moscow');

export const baseUrl = navigations['uds-moscow'];

export const URLs = {
  root: {
    url: navigations['uds-moscow.app'],
  },
  institutions: {
    url: navigations['link.uds-moscow.app.institutions'],
  },
  sections: {
    url: navigations['link.uds-moscow.app.sections'],
  },
  join: {
    url: navigations['link.uds-moscow.app.join'],
  },
};
