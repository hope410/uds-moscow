import { Action } from 'redux';
import * as types from '../../../action-types';
import Region from '../../../models/Region';

export interface RegionsState {
  data: Region[];
}

export interface RegionsAction extends Action<string> {
  payload: {
    data: Region[];
  };
}

const initialState: RegionsState = {
  data: null,
};

const handleRegionsGetData = (state: RegionsState, action: RegionsAction) => ({
  ...state,
  data: action.payload.data,
});

const handlers = {
  [types.REGIONS_GET_DATA]: handleRegionsGetData,
};

export default function (
  state = initialState,
  action: RegionsAction
): RegionsState {
  const handler = handlers[action.type];
  return handler ? handler(state, action) : state;
}
