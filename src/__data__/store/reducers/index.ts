import { combineReducers } from 'redux';

import regionsReducer, { RegionsState } from './regions';
import competitionsReducer, { CompetitionsState } from './competitions';

export interface RootState {
  regions: RegionsState;
  competitions: CompetitionsState;
}

export default combineReducers<RootState>({
  regions: regionsReducer,
  competitions: competitionsReducer,
});
