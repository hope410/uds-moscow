import { Action } from 'redux';
import * as types from '../../../action-types';
import Competition from '../../../models/Competition';

export interface CompetitionsState {
  data: Competition[];
}

export interface CompetitionsAction extends Action<string> {
  payload: {
    data: Competition[];
  };
}

const initialState: CompetitionsState = {
  data: null,
};

const handleCompetitionsGetData = (
  state: CompetitionsState,
  action: CompetitionsAction
) => ({
  ...state,
  data: action.payload.data,
});

const handlers = {
  [types.COMPETITIONS_GET_DATA]: handleCompetitionsGetData,
};

export default function (
  state = initialState,
  action: CompetitionsAction
): CompetitionsState {
  const handler = handlers[action.type];
  return handler ? handler(state, action) : state;
}
