const router = require('express').Router();

router.post('/login', (req, res) => {
  res.send(require('./mocks/login/success'));
  // res.status(500).send({"code":2,"error":"Не верный логин или пароль"});
  // res.status(400).send({ message: 'Закрыто на обед!' });
});

router.post('/confirm/email', (req, res) => {
  res.send(require('./mocks/confirm/email/success'));
  // res.status(400).send(require('./mocks/confirm/email/error'));
});

router.post('/register', (req, res) => {
  setTimeout(() => {
    res.send(require('./mocks/register/success'));
    // res.status(400).send(require('./mocks/register/error'));
  }, 2000);
});

router.post('/recover/password/new', (req, res) => {
  res.send(require('./mocks/recover/password/new/success'));
  // res.status(400).send(require('./mocks/recover/password/new/error'));
});

router.post('/recover/password/confirm', (req, res) => {
  res.send(require('./mocks/recover/password/confirm/success'));
  // res.status(400).send(require('./mocks/recover/password/confirm/error'));
});

router.post('/recover/password', (req, res) => {
  res.send(require('./mocks/recover/password/success'));
  // res.send(require('./mocks/recover/password/error'));
});

router.post('/recover/login/new', (req, res) => {
  res.send(require('./mocks/recover/login/new/success'));
  // res.status(400).send(require('./mocks/recover/login/new/error'));
});

router.post('/recover/login/confirm', (req, res) => {
  res.send(require('./mocks/recover/login/confirm/success'));
  // res.status(400).send(require('./mocks/recover/login/confirm/error'));
});

router.post('/recover/login', (req, res) => {
  res.send(require('./mocks/recover/login/success'));
  // res.send(require('./mocks/recover/login/error'));
});

router.get('/regions', (req, res) => {
  res.send(require('./mocks/data/regions'));
});

router.get('/competitions', (req, res) => {
  res.send(require('./mocks/data/competitions'));
});

module.exports = router;
