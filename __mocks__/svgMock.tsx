import React from 'react';

module.exports = {
  ReactComponent: React.forwardRef(
    (props, ref: React.LegacyRef<HTMLSpanElement>) => (
      <span ref={ref} {...props} />
    )
  ),
};
