/// <reference types="cypress" />

describe('Главная страница', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('location.pathname = /uds-moscow/', () => {
    cy.location().should((location) => {
      expect(location.pathname).to.eq('/uds-moscow/');
    });
  });

  it('Отрисован блок "Для самых маленьких"', () => {
    cy.get('[data-cy="littleones"]').should('exist');
  });

  it('Работает выбор региона в блоке "Для самых маленьких"', () => {
    cy.get('[data-cy="select-input-container"]').should('exist');
    cy.get('[data-cy="select-input-container"]').click();
    cy.get('[data-cy="select-input-dropdown"]').should('exist');
    cy.get('#select-input-group__1').should('exist');
    cy.get('#select-input-group__1').click();
    cy.get('#select-input-group__1 > [data-cy="select-input-group-items"]').should('exist');
    cy.get('#select-input-group__1 > [data-cy="select-input-group-items"] > [data-cy="select-input-item"]#select-input-item__0').should('exist');
    cy.get('#select-input-item__0').click();

    cy.get('[data-cy="select-input-label"]').should('have.value', 'Богородское');
  });

  it('Отрисован блок "Клубы и студии"', () => {
    cy.get('[data-cy="clubs-and-studies"]').should('exist');
  });

  it('Отрисован блок "Соревнования"', () => {
    cy.get('[data-cy="competitions"]').should('exist');
  });
});
